//function to check the sign up form is valid
function signupForm(id) {

	//grab the id of the form
	var theForm = document.getElementById('signupForm');

	//check all the inputs for data
	if (theForm.firstname.value.length == 0 || theForm.lastname.value.length == 0 || 
		theForm.email.value == 0 || theForm.password.value.length < 6) 
	{
		   //if the first name isn't valid 	
		   if (theForm.firstname.value.length == 0) {
			  //use the span id to generate a message and do not let the form submit
		      firstnameCheck.innerHTML = 'Please enter a firstname!';
		    } else {
			    firstnameCheck.innerHTML = '';
		    }

		    //if the first name isn't valid 	
		    if (theForm.lastname.value.length == 0) {
			  //use the span id to generate a message and do not let the form submit
		      lastnameCheck.innerHTML = 'Please enter a lastname!';
		    } else {
			    lastnameCheck.innerHTML = '';
		    }

		    //make variables to validate email
		    var eml = theForm.email.value;
		    var atpos=eml.indexOf("@");
			var dotpos=eml.lastIndexOf(".");
			
			//create if statement to check email
			if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= eml.length) {
			  //use the span id to generate a message and do not let the form submit
		      emailCheck.innerHTML = 'Please enter a valid email!';
		    } else {
			  emailCheck.innerHTML = '';
		    }

		    //if the first name isn't valid 	
		    if (theForm.password.value.length < 6) {
			  //use the span id to generate a message and do not let the form submit
		      passwordCheck.innerHTML = 'Your password must be 6 characters long!';
		    } else {
			    passwordCheck.innerHTML = '';
		    }

		    theForm.focus();
			return false;   
	}

	return true;

}//signForm Function

//function to check the login up form is valid
function loginForm(id) {

	//grab the id of the form
	var theForm = document.getElementById('loginForm');

	//check all the inputs for data
	if (theForm.email.value == 0 || theForm.password.value.length < 6) 
	{
		    //make variables to validate email
		    var eml = theForm.email.value;
		    var atpos=eml.indexOf("@");
			var dotpos=eml.lastIndexOf(".");
			
			//create if statement to check email
			if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= eml.length) {
			  //use the span id to generate a message and do not let the form submit
		      emailCheck.innerHTML = 'Please enter a valid email!';
		    } else {
		      //set to empty string
			  emailCheck.innerHTML = '';
		    }

		    //if the first name isn't valid 	
		    if (theForm.password.value.length < 6) {
			  //use the span id to generate a message and do not let the form submit
		      passwordCheck.innerHTML = 'Your password is incorrect!';
		    } else {
		    	//set to empty string
			    passwordCheck.innerHTML = '';
		    }

		    //validation has failed so pull in errors
		    theForm.focus();
			return false;   
	}

	//if passed the errors reuturn true
	return true;

}//loginForm Function

//function to check the schedule form is valid
function scheduleForm(id) {

	//grab the id of the form
	var theForm = document.getElementById('scheduleForm');

	//check all the inputs for data
	if (theForm.sessionname.value.length == 0 || theForm.date.value.length == 0) 
	{
		   //if the first name isn't valid 	
		   if (theForm.sessionname.value.length == 0) {
			  //use the span id to generate a message and do not let the form submit
		      sessionnameCheck.innerHTML = 'Please enter a session name!';
		    } else {
		    	//set to nothing
			    sessionnameCheck.innerHTML = '';
		    }

		    //if the first name isn't valid 	
		    if (theForm.date.value.length == 0) {
			  //use the span id to generate a message and do not let the form submit
		      dateCheck.innerHTML = 'Please enter a date!';
		    } else {
		    	//set to nothing
			    dateCheck.innerHTML = '';
		    }

		    //failed validation pull errors
		    theForm.focus();
			return false;   
	}
	//passed validation
	return true;

}//schedleForm Function

//jquery document ready function
 $(document).ready(function(){
 	
 	//JS for Isotope filtering on demonstration section
 	//cache container
 	var $container = $('#exerciseList');
 	 //initialize isotope
 	$container.isotope({
 	   //options...
 	  animationOptions: {
 	     duration: 750,
 	     easing: 'linear',
 	     queue: false
 	   }
 	});

	//tabs on progression
	$('.progression-tabs').each(function(){
	// For each set of tabs, we want to keep track of
	// which tab is active and it's associated content
	var $active, $content, $links = $(this).find('a');

		// If the location.hash matches one of the links, use that as the active tab.
		// If no match is found, use the first link as the initial active tab.
		$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
		$active.addClass('current-tab');
		$content = $($active.attr('href'));

		// Hide the remaining content
		$links.not($active).each(function () {
			$($(this).attr('href')).hide();
		});

		// Bind the click event handler
		$(this).on('click', 'a', function(e){
		// Make the old tab inactive.
		$active.removeClass('current-tab');
		$content.fadeOut(400);

		// Update the variables with the new link and content
		$active = $(this);
		$content = $($(this).attr('href'));

		// Make the tab active.
		$active.addClass('current-tab');
		$content.fadeIn(1000);

		// Prevent the anchor's default click action
		e.preventDefault();
	  });
	});	


	//datepicker in add session section
	$('.datepicker').pickadate();

});//document ready function