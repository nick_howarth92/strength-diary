<?php 
class GoogleCalendarController extends BaseController {


	public function addToCalendar() {
		//require the correct files
		require_once 'Google/Client.php';
		require_once 'Google/Service/calendar.php';

		//set up a new client 
		$client = new Google_Client();
	  	//give the application a name
		$client->setApplicationName("Strength Diary");
	  	//set the client ID
		$client->setClientID("261123229521.apps.googleusercontent.com");
	  	//set the client secret
		$client->setClientSecret("hdepQCM2uAtN7hlNhCK34Fz7");
	  	//set the re-direct link
		$client->setRedirectUri("http://strengthdiary.dev");
	 	//set the devloper key
	 	$client->setDeveloperKey("AIzaSyB8z_TgBe2Ir25tHfHbm4eTmdeBCdL_X5c");
		//set the scoper
	 	$client->setScopes(array('https://www.googleapis.com/auth/calendar'));
	    
	  	//create a new service passing client information
	  	$calendar = new Google_Service_Calendar($client);
	 
	       // get the code returned from the page
	        if (isset($_GET['code'])) {
	 
	                // authenticate the code with the client
	                $client->authenticate($_GET['code']);
	 
	                // set the token session of the authenticate code
	                Session::put('token', $client->getAccessToken());
	 
	                // redirect onto self. to head to the next section.
	                header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
	        }
	 
	        if (Session::has('token')) {
	                // if there is a token set the access of the client
	                $client->setAccessToken(Session::get('token'));
	        }
	 
	        if ($client->getAccessToken()){

	        	//get the user currently logged in
				$userID = Auth::user()->id;
	        	//get all the sessions based on a user id
		   		$sessions = Sesh::where('user_id', '=', $userID)->get();

		   		foreach ($sessions as $session) {
		   			$sessionName = $session->name;
		   			$sessionDate = $session->date;
		   		
		            // create a new event
		            $event = new Google_Event();
		           
		            // set summary
		            $event->setSummary($sessionName);
		           
		            // set location
		            $event->setLocation('The Gym');
		           
		            // add date and time for start
		            $start = new Google_EventDateTime();
		            $start->setDateTime($sessionDate . 'T10:00:00.000-05:00');
		            $event->setStart($start);
		           
		            // add date and time for end
		            $end = new Google_EventDateTime();
		            $end->setDateTime($sessionDate . 'T10:00:00.000-05:00');
		            $event->setEnd($end);

		            // insert into the primary calendar
		            $createdEvent = $cal->events->insert('primary', $event);
	 			
	 			}//end foreach

	        } else {
	                $authUrl = $client->createAuthUrl();
	                echo "<h2><a href='$authUrl'>Connect Me!</a></h2>";
	        }
	}//addToCalendar

}