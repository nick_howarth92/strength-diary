<?php

class AppScheduleController extends BaseController {

	/**
	 *
	 * Returning the view with needed variables
	 *
	 * @return app.schedule view
	 *
	*/
	public function index()
	{	
		//get the user currently logged in
		$userID = Auth::user()->id;

		//querying tables using models
	    $bodyParts = BodyPart::all();
	    $muscles = Muscle::all();
	    $exercises = Exercise::all();
	    $indivExercises = Exercise::all();
	    //get all the sessions based on a user id
	    $sessions = Sesh::where('user_id', '=', $userID)->get();
	    //get all the exercises planned based on a user id
	    $exercisesPlanned = ExercisePlanned::where('user_id', '=', $userID)->get();

	    //return to the view all the variables needed
		return View::make('app.schedule')
					->with('bodyParts', $bodyParts)
					->with('muscles', $muscles)
					->with('exercises', $exercises)
					->with('indivExercises', $indivExercises)
					->with('sessions', $sessions)
					->with('exercisesPlanned', $exercisesPlanned);
	}

	/**
	 *
	 * Grab the id of the muscle passed and return it using JSON
	 * @param int $id
	 * @return $muscles
	 *
	*/
	public function showMuscles($id) 
	{	
		// Get the muscle based on a body part
		$muscles = Muscle::where('body_part_id', '=', $id)->get();
		return $muscles;
	}

	/**
	 *
	 * Grab the id of the exercise passed and return it using JSON
	 * @param int $id
	 * @return $exercises
	 *
	*/
	public function showExercises($id) 
	{
		// Get the primary muscle of the exercise
		$exercises = Exercise::where('primary_muscle', '=', $id)->get();
		return $exercises;
	}

	/**
	 *
	 * Grab the id of the exercise passed and return it using JSON
	 * @param int $id
	 * @return $indivExercises
	 *
	*/
	public function showIndivExercise($id) 
	{
		$indivExercises = Exercise::where('id', '=', $id)->get();
		return $indivExercises;
	}

	/**
	 *
	 * Adds a session for the user
	 * @return app.schedule
	 *
	*/
	public function addSession() {

		//get the user currently in the session
		$userID = Auth::user()->id;

		//get the input fields
		$sessionType = Input::get('activity-type');
		$sessionName = Input::get('sessionname');
		$date = Input::get('date');
		$notes = Input::get('notes');
		$exerciseIDs = Input::get('exerciseID');

		//validate the form
		$validator = Validator::make(
		    array(
		    	//set the inputs in question to the variables requested
		        'sessionname' => $sessionName,
		        'date' => $date,
		        'exerciseID' => $exerciseIDs
		    ),
		    array(
		    	//set validation for the variables
		        'sessionname' => 'required|min:2',
		        'date' => 'required|min:2',
		        'exerciseID' => 'required'
		    ),
		    array(
		    	//create custom error messages for the errors
		    	'sessionname.required' => 'please choose a session name!',
		    	'date.required' => 'please choose a date for the session!',
		    	'exerciseID.required' => 'You need to choose at least one exercise for a session!',
		    )
		);//validator

		//if the validation fails
		if ($validator->fails())
		{	
			//if there are errors re-direct with the errors
		    return Redirect::to('schedule')->withErrors($validator);
		}

		//convert the date to an appropiate format
		$date = date("Y-m-d", strtotime($date));

		//insert the data through the model
		$sessionSQL = Sesh::create(array('user_id' => $userID,
										 'title' => $sessionName,
										 'notes' => $notes,
										 'scheduled_for' => $date,
		
								   ));
		//get the last inserted ID
		$sessionID = $sessionSQL->id;

		//loop around all the exercises that were inserted in the form
		foreach ($exerciseIDs as $exerciseID) {
			//insert the data through the model
			$exerciseListSQL = ExercisePlanned::create(array('user_id' => $userID,
															 'session_id' => $sessionID,
															 'exercise_id' => $exerciseID,
															 'type' => $sessionType
													));

		}//foreach		

		//return if successfully inserted with message to notify user
		return Redirect::to('schedule')->with('addmessage', 'Session Saved!');

	}//addSession	

	/**
	 *
	 * Adds a score for an exercise for the user
	 * @return app.schedule
	 *
	*/
	public function addScore() {

		//get the user currently in the session
		$userID = Auth::user()->id;

		//get the input fields
		$exerciseID = Input::get('exerciseID');
		$effort = Input::get('effort');
		$weights = Input::get('weight');
		$reps = Input::get('reps');
			
		//declare variables for the for loop
		$sets = 0;
		$totalScore = 0;
		$totalWeight = 0;
		$totalReps = 0;

		//loop around the sets passed by the user
		for ($i = 0; $i < sizeof($reps); $i++) {

			//loops round reps and weights
			$rep = $reps[$i];
			$weight = $weights[$i];

			//if the weight submitted is numeric
			if (is_numeric($weight)) 
			 {	
			 	//validation passed
			 	echo "passed validation<br>";
			 } else {
			 	//redirect with errors
			 	return Redirect::to('schedule')->with('wrerror', 'Failed to save score... Make sure you enter numeric values e.g (1, 2, 3)');
			 }

			 //if the rep is submitted is numeric
			 if (is_numeric($rep)) 
			 {	
			 	//validation passed
			 	echo "passed validation<br>";
			 } else {
			 	//redirect back with errors
			 	return Redirect::to('schedule')->with('wrerror', 'Failed to save score... Make sure you enter numeric values e.g (1,2,3)');
			 }

			//add on to the sets counter
			$sets++;

			//gets the total weight and reps
			$totalWeight += $weight;
			$totalReps += $rep;

			//works out the score based on reps, weight and effort
			$score[] = (($weight * $rep)/$effort);
		}

		//if the score is empty
		if(!empty($score)) {
			//loop around the score and add to the total score
			foreach ($score as $score) {
				$totalScore += $score;
			}
		} else {
			//error with score, redirect back
			return Redirect::to('schedule')->with('scorerror', 'Failed to save score... You Must add at least one set!');
		}	

		//divide the total score by how many sets submitted
		$finalScore = $totalScore / $sets;

		//insert the data through the model
		$scoreSQL = Entry::create(array('user_id' => $userID,
										'exercise_planned_id' => $exerciseID,
										'total_weight' => $totalWeight,
										'total_reps' => $totalReps,
										'effort' => $effort,
										'score' => $finalScore
			
									  ));

		//return if successfully inserted with message to notify user
		return Redirect::to('schedule')->with('scoremessage', 'Score Saved!');
 
	}//addScore

}

?>