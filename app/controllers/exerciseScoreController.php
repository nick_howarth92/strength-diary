<?php

class exerciseScoreController extends BaseController {


	public function index($id)
	{	
		$exercisePlannedID = ExercisePlanned::where('id', '=', $id)->first();

		return View::make('app.exercisescore')
					 ->with('exercisePlannedID', $exercisePlannedID);
	}

}