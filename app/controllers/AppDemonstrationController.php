<?php

class AppDemonstrationController extends BaseController {

	public function index()
	{	
		//querying tables using models
        $bodyParts = BodyPart::all();
        $exercises = Exercise::all();
        $muscles = Muscle::all();

		//returns to the view passing in variable holding queries
		return View::make('app.demonstration')
					->with('bodyParts', $bodyParts)
					->with('exercises', $exercises)
					->with('muscles', $muscles);

		
	}

	//function to grab the id of the exercise passed and return it using JSON
	public function showExercises($id) 
	{
		$exercises = Exercise::where('body_part_id', '=', $id)->get();
		return $exercises;
	}

	//function to grab the id of the exercise passed and return it using JSON
	public function showMuscles($id) 
	{
		$muscles = Muscle::where('body_part_id', '=', $id)->get();
		return $muscles;
	}

}

?>