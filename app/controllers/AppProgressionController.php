<?php

class AppProgressionController extends BaseController {


	public function index()
	{
		//get the user currently in the session
		$userID = Auth::user()->id;

		//querying tables using models
        $bodyParts = BodyPart::all();
        $muscles = Muscle::all();
        $exercises = Exercise::all();
        $entries = Entry::where('user_id', '=', $userID)->get();
        $sessions = Sesh::where('user_id', '=', $userID)->get();
        $exercisesPlanned = ExercisePlanned::where('user_id', '=', $userID)->get();
        //get the highest score from the entry tables
        $highestEntry = Entry::orderBy('score', 'DESC')->where('user_id', '=', $userID)->first();
        
        $compoundExercises = Exercise::where('id', '=', 15)
        								->orWhere('id', '=', 19)
        								->orWhere('id', '=', 31)
        								->orWhere('id', '=', 32)
        								->orWhere('id', '=', 44)
        								->orWhere('id', '=', 81)
        								->get();

		return View::make('app.progression')
					->with('bodyParts', $bodyParts)
					->with('exercises', $exercises)
					->with('muscles', $muscles)
					->with('compoundExercises', $compoundExercises)
					->with('entries', $entries)
					->with('exercisesPlanned', $exercisesPlanned)
					->with('sessions', $sessions)
					->with('highestEntry', $highestEntry);
	}

	//function to grab the id of the muscle passed and return it using JSON
	public function showMuscles($id) 
	{	
		//get all the muscles where the body_part is equal to the one passed
		$muscles = Muscle::where('body_part_id', '=', $id)->get();
		return $muscles;
	}

	//function to grab the id of the exercise passed and return it using JSON
	public function showExercises($id) 
	{	
		//get all the exercises where the primary_muscle is equal to the one passed
		$exercises = Exercise::where('primary_muscle', '=', $id)->get();
		return $exercises;
	}

	//function to grab the id of the exercise passed and return it using JSON
	public function exerciseScore($id, $type) 
	{	
		//get the user currently in the session
		$userID = Auth::user()->id;

		//get all the entries for a user
		$entries = Entry::where('user_id', '=', $userID)->get();

		//set exercise scores array to blank
		$exerciseScores = '';

		//loop around all the entries
		foreach ($entries as $entry) {
			//get all the entries with a exercise planned
			$exercise = $entry->exercisePlanned->exercise_id;

			//pull date from database entries created_at
			$date = $entry->created_at;

				//if the type equals the entry types
				if($type == $entry->exercisePlanned->type):
					//if the exercise is equal to the id pull the score into an array
					if($exercise == $id):
						//exercisescore array
						$exerciseScores[] = array(
							'date' => date("M", strtotime($date)),
							'score' => $entry->score,
						);
					endif;
				endif;


		}//for loop
		
		return $exerciseScores;
	}

}
