<?php

class LogoutController extends BaseController {

	public function index()
	{
		return View::make('app.logout');
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::to('/');
	}

}