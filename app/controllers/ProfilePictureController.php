<?php

class ProfilePictureController extends BaseController {


	public function index()
	{	

		return View::make('app.editprofilepicture');
	}

	public function uploadImage()
	{
		//put the logged in user into a variable
		$userID = Auth::user()->id;

		//get the users profile picture
		$profile_picture = Auth::user()->profile_picture;

		//store the users file path in a variable
		$filePath = "uploads/$userID/$profile_picture";

		//if the profile picture field isn't empty
		if(!empty($profile_picture)):
			//delete any files in the directort
			if (!unlink($filePath)):
			  echo "Error deleting $profile_picture";
			else:
			  echo "Updating $profile_picture<br>";
			endif;
		endif;

		//if the input file exsists
		if (Input::hasFile('photo')):
    		//get the file and it's original name
			$file = Input::file('photo')->getClientOriginalName();
			
			//move the file to the unique user id directory
			Input::file('photo')->move('uploads/'. $userID .'/', $file);

			//update the data through the model
			$sql = User::where('id', '=', $userID)->update(array('profile_picture' => $file));

			//redirect to profile passing message
			return Redirect::to('profile')->with('message', 'Profile Picture Updated!');
		endif;


	}

}