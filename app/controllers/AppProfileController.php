<?php

class AppProfileController extends BaseController {

	public function index()
	{	
		if(!empty(Auth::user()->id)):
			//put the logged in user into a variable
			$userID = Auth::user()->id;
		else:
			return Redirect::to('login');
		endif;
		
		//get all the sessions
		$sessions = Sesh::where('user_id', '=', $userID)->get();

		//make views and pass content back
		return View::make('app.profile')
					->with('sessions', $sessions);
	}

}

?>