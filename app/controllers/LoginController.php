<?php

class LoginController extends BaseController {

	public function index()
	{
		return View::make('front.login');
	}

	public function login()
	{

		//get the email and password
		$email = Input::get('email');
		$password = Input::get('password');

		//check if the username(email) and password match and keep user logged in
		if (Auth::attempt(array('email' => $email, 'password' => $password), true))
		{
		    return Redirect::to('profile');
		} else 
		{
			return Redirect::to('login')
							->with('message', 'You entered and incorrect email or password!');;
		}

	}

}