<?php

class showVideoController extends BaseController {


	public function index($id)
	{	
		$exercises = Exercise::where('id', '=', $id)->first();

		return View::make('app.showvideo')
					->with('exercises', $exercises);
	}

}