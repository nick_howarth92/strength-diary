<?php

class SignupController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	*/

	public function index()
	{	
		//make the view for signup
		return View::make('front.signup');
	}

	public function signup()
	{

		//get the input fields
		$firstname = Input::get('firstname');
		$lastname = Input::get('lastname');
		$email = Input::get('email');
		$password = Input::get('password');

		//validate the form
		$validator = Validator::make(
		    array(
		    	//pull in data needed to be valdidated
		        'firstname' => $firstname,
		        'lastname' => $lastname,
		        'email' => $email,
		        'password' => $password
		    ),
		    array(
		    	//set validation
		        'firstname' => 'required|min:3',
		        'lastname' => 'required|min:3',
		        'email' => 'email|required|unique:users',
		        'password' => 'required|min:6'
		    ),
		    array(
		    	//create errors for validation
		    	'firstname.required' => 'please enter your first name!',
		    	'lastname.required' => 'please enter your last name!',
		    	'email.email' => 'please enter a valid email address!',
		    	'password.required' => 'please enter a valid password!',
		    )
		);//validator

		if ($validator->fails())
		{
		    return Redirect::to('signup')->withErrors($validator);
		}

		//hash the users password
		$password = Hash::make($password);

		//insert the data through the model
		$signupSQL = User::create(array('first_name' => $firstname,
										'second_name' => $lastname,
										'email' => $email,
										'password' => $password,
										'profile_picture' => ''
			
								  ));
		//Redirect to login
		Redirect::to('login');

		
	}

}