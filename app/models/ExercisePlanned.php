<?php

class ExercisePlanned extends Eloquent {
    // Variable to define the table used by the model.
	protected $table = 'exercises_planned';
    // Variable to define the columns that can be edited by the model.
	protected $fillable = array('id', 'user_id', 'session_id', 'exercise_id', 'type',
                                'created_at', 'updated_at');

    /*
     * Return the user that belongs to the exercice planned referenced.
    */
    public function user()
    {
        return $this->belongsTo('User');
    }

	/*
     * Return the exercises that belong to the exercise planned referenced.
    */
    public function exercise()
    {
        return $this->belongsTo('Exercise');
    }

    /*
     * Return the session that belongs to the exercise planned referenced.
    */
    public function session()
    {
        return $this->belongsTo('Sesh');
    }

    /*
     * Return the one entry in reference to the exercise planned.
    */
     public function entry()
    {
        return $this->hasOne('Entry');
    }

}