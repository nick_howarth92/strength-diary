<?php

class Entry extends Eloquent {
  // Variable to define the table used by the model.
	protected $table = 'entries';
  // Variable to define the columns that can be edited by the model.
	protected $fillable = array('id', 'user_id', 'exercise_planned_id', 'total_weight', 'total_reps', 
                                'effort', 'score', 'created_at', 'updated_at');

  /*
     * Return the user that belongs to the entry referenced.
  */
	public function user()
    {
        return $this->belongsTo('User');
    }

	/*
     * Return the exercises planned that belong to the entry referenced.
  */
   public function exercisePlanned() 
   	{
        return $this->belongsTo('ExercisePlanned');
    }

}