<?php

class Muscle extends Eloquent {
    // Variable to define the table used by the model.
	protected $table = 'muscle';
    // Variable to define the columns that can be edited by the model.
	protected $fillable = array('id', 'body_part_id', 'name', 'slug', 'created_at', 'updated_at');


	/*
     * Return the body parts that belong to the muscle referenced.
    */
	public function bodyPart()
    {
        return $this->belongsTo('BodyPart', 'body_part_id');
    }

    /*
     * Return all exercises which include the muscle referenced.
    */
    public function exercises() 
    {
    	return $this->hasMany('Exercise');
    }


}