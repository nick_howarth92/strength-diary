<?php

class Sesh extends Eloquent {
    // Variable to define the table used by the model.
	protected $table = 'sessions';
    // Variable to define the columns that can be edited by the model.
	protected $fillable = array('id', 'user_id', 'title', 'notes', 'scheduled_for',
                                'created_at', 'updated_at');

    /*
     * Return the user that belongs to the session referenced.
    */
	public function user()
    {
        return $this->belongsTo('User');
    }

	/*
     * Return all exercises planned which include the session referenced.
    */
    public function exercisePlanned()
    {
        return $this->hasMany('ExercisePlanned');
    }

}