<?php
//include the appropiate 
use Illuminate\Auth\UserInterface;

class User extends Eloquent implements UserInterface {
    // Variable to define the table used by the model.
	protected $table = 'users';
    // Variable to define the columns that can be edited by the model.
	protected $fillable = array('id', 'first_name', 'second_name', 'email', 
                                'password', 'profile_picture', 'created_at', 'updated_at');

	/*
     * Return all session planned which include the user referenced.
    */
    public function session()
    {
        return $this->hasMany('Sesh');
    }

    /*
     * Return all exercise planned which include the user referenced.
    */
    public function exercisePlanned()
    {
        return $this->hasMany('ExercisePlanned');
    }

    /*
     * Return all entries planned which include the user referenced.
    */
    public function entry()
    {
        return $this->hasMany('Entry');
    }

    /*
     * Get the email for the user referenced.
    */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /*
     * Get the password for the user referenced.
    */
     public function getAuthPassword()
    {
        return $this->password;
    }


}
