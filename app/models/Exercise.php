<?php

class Exercise extends Eloquent {
	protected $table = 'exercises';
	protected $fillable = array('id', 'name', 'body_part_id', 'primary_muscle', 
								'secondary_muscle', 'third_muscle', 'fourth_muscle', 
								'demonstration_url', 'created_at', 'updated_at');


    /*
     * Return the muscles that belong to the exercise referenced.
    */
	public function firstMuscles()
    {
        return $this->belongsTo('Muscle', 'secondary_muscle', 'third_muscle', 'fourth_muscle');
    }

    /*
     * Return the muscles that belong to the exercise referenced.
    */
    public function secondMuscles()
    {
        return $this->belongsTo('Muscle', 'secondary_muscle', 'third_muscle', 'fourth_muscle');
    }

    /*
     * Return the muscles that belong to the exercise referenced.
    */
    public function thirdMuscles()
    {
        return $this->belongsTo('Muscle', 'secondary_muscle', 'third_muscle', 'fourth_muscle');
    }

    /*
     * Return the muscles that belong to the exercise referenced.
    */
    public function fourthMuscles()
    {
        return $this->belongsTo('Muscle', 'secondary_muscle', 'third_muscle', 'fourth_muscle');
    }

    /*
     * Return the body parts that belong to the exercise referenced.
    */
    public function bodyPart() {
    	return $this->belongsTo('BodyPart');
    }

    /*
     * Return all exercises planned which include the exercise referenced.
    */
    public function exercisesPlanned() {
        return $this->hasMany('ExercisesPlanned');
    }

}
