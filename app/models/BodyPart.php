<?php

class BodyPart extends Eloquent {
    // Variable to define the table used by the model.
	protected $table = 'body_part';
    // Variable to define the columns that can be edited by the model.
	protected $fillable = array('id', 'name', 'created_at', 'updated_at');

	/*
     * Return all muscles which include the body part referenced.
    */
	public function muscles()
    {
        return $this->hasMany('Muscle');
    }

    /*
     * Return all exercises which include the body part referenced.
    */
    public function exercise()
    {
        return $this->hasMany('Exercise');
    }


}