@extends('front.master')

@section('main')

<div class="signup-container">

	<h4 class="t-center t-black">Sign up and get started!</h4>
	<div class="thin-line"></div>

	<form method="post" id="signupForm" action="signup-submission" onsubmit="return signupForm()">
		
		<!--pull in erorrs from controller-->
		<div class="errors">
				<?php echo $errors->first('firstname'); ?>
		</div>
		
		<!--pull in erorrs using javaScript-->
		<span class="errors" id="firstnameCheck"></span>

		<p>
			<input type="text" class="fullwidth-input" name="firstname" placeholder="First Name..." required />
		</p>
		
		<!--pull in erorrs from controller-->
		<div class="errors ">
				<?php echo $errors->first('lastname'); ?>
		</div>
		
		<!--pull in erorrs using javaScript-->
		<span class="errors" id="lastnameCheck"></span>

		<p>
			<input type="text" class="fullwidth-input" name="lastname" placeholder="last Name..." required />
		</p>
		
		<!--pull in erorrs from controller-->
		<div class="errors ">
				<?php echo $errors->first('email'); ?>
		</div>

		<!--pull in erorrs using javaScript-->
		<span class="errors" id="emailCheck"></span>

		<p>
			<input type="email" class="fullwidth-input" name="email" placeholder="Email..."  required />
		</p>
		
		<!--pull in erorrs from controller-->
		<div class="errors ">
				<?php echo $errors->first('password'); ?>
		</div>

		<!--pull in erorrs using javaScript-->
		<span class="errors" id="passwordCheck"></span>

		<p>
			<input type="password" class="fullwidth-input" name="password" placeholder="Password..." required />
		</p>

		<div class="s-center">	
			<input class="m-top" type="submit" value="Sign up!" />
		</div>

	</form>

</div>

@endsection