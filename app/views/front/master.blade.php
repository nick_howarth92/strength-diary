<!doctype html>
<html>
<head>
	<title>Home | Strength Diary</title>
	
	<!--google fonts-->
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,700' rel='stylesheet' type='text/css' />
	
	<!--Main Stylesheets-->
	{{ HTML::style('css/normalize.css') }}
	{{ HTML::style('css/global.css') }}
	{{ HTML::style('css/front.css') }}

	<!--Modernizer JS fix for IE 8-->
	{{ HTML::script('js/modernizer/modernizer.js') }}
	<!--Media queries IE 8-->
	{{ HTML::script('js/css3mediaqueries/css3-mediaqueries.js') }}

	<!--js libray-->
	{{ HTML::script('http://code.jquery.com/jquery-1.10.2.min.js') }}

</head>

<body>

	<header>
		<div class="container">
			
			<a href="/">
				<h1 class="logo">
					<img src="images/logo.png" alt="Strength Diary" />
				</h1>
			</a>
			
			<nav>
				<ul> 
					<li><a class="signup" href="signup">Sign Up</a></li>
					<li><a href="login">Login</a></li>
				</ul>
			</nav>
		</div><!--header-container-->
	</header>

	<div class="container wrapper">
		@yield('main')
	</div>

<!--Main JS-->
{{ HTML::script('js/main.js') }}

</body>


</html>