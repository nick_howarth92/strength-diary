@extends('front.master')

@section('main')
		<div class="features">
			<div class="progress-icon icon-col">
				<div class="lead-icon typicons"><i class="typicons-lineChart"></i></div>
				<h3>Progression</h3>
				<p>Track your progression with the in built scoring mechanism</p>
				<br class="clear">
			</div>

			<div class="schedule-icon icon-col">
				<div class="lead-icon"><i class="fontawesome-calendar"></i></div>
				<h3>Schedule</h3>
				<p>Accuratly plan workouts and view on multiple platforms</p>
				<br class="clear">
			</div>

			<div class="demonstration-icon">
				<div class="lead-icon"><i class="entypo-video"></i></div>
				<h3>Demonstration</h3>
				<p>Find new workouts with video demonstrations!</p>
				<br class="clear">
			</div>
		</div><!-- /features-->

		<br class="clear" />

			<h2><div class="small-linebreak"></div>Get Started Now!</h2>
			
			<p class="t-center">
				<a class="button signup" href="signup">Sign Up</a>
			</p>
		
		<br class="clear" />
@endsection