@extends('front.master')

@section('main')

<div class="login-container">
	<h4 class="t-center t-black">Login to your account</h4>
	<div class="thin-line"></div>

	<form method="post" id="loginForm" action="login" onsubmit="return loginForm()">
	
	<!--pull in error when logging in user-->
	<?php $loginError =  Session::get('message'); ?>
		<!--div to hold error-->
		<div class="login-error">{{$loginError}}</div>
		
		<!--pull in erorrs using javaScript-->
		<span class="errors" id="emailCheck"></span>

		<p>
			<input type="email" class="fullwidth-input" name="email" placeholder="Email..."required/>
		</p>
		
		<!--pull in erorrs using javaScript-->
		<span class="errors" id="passwordCheck"></span>

		<p>
			<input type="password" class="fullwidth-input" name="password" placeholder="Password..." required />
		</p>

		<div class="s-center">	
			<input class="m-top" type="submit" value="Login" />
		</div>

		</form>
</div>

@endsection