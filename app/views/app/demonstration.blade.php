@extends('app.master')

@section('main')

	<div class="container profile-margin margin-top">

		<div class="demo-filtercontainer">
		
			<div class="humanbody">
				<img src="{{ asset('images/demonstration/humanbody-full.png') }}" 
				alt="human body diagram" />	
			
			</div><!--human-body-->

			<div class="demo-form">
				<form method="post" action="#">
					<select id="bodyPart" name="bodyPart">
							<option>Filter Body Part</option>
							<!--loop through all the body parts-->
							@foreach($bodyParts as $bodyPart)
								<option value="{{ $bodyPart->id }}">
								<!--display in option values-->
									{{ $bodyPart->name }}
								</option>
							@endforeach
					</select>
				</form>
			</div><!--demo-form-->

		</div><!--demonstration-filtercontainer-->	

		<!--div to pull all Filters into-->
		<div id="filters">
			<ul>
			</ul>
		</div>
		
		<!--pulls exercises in-->
		<div id="exerciseList">

		</div>

						

</div><!--container-->

<script>
//detect on change of the select box
$('#bodyPart').change(function() {

// get value from the select box	
var id = $('#bodyPart').val(),
	//get all the muscles 
	muscles = <?php echo $muscles; ?>,
	primaryMuscle,
	secondMuscle,
	thirdMuscle,
	fourthMuscle,
	filterMuscle,
	//set up a variable for current exercises
	oldExercises = $('#exerciseList div');

	 // remove all children in the div currently (if any)
	 $('#exerciseList').isotope('remove', oldExercises );
	 //remove the filters 
	 $('#filters ul li').remove();

	// get the filtered Primary Muscle using the bodyPart
	$.getJSON("/demonstration/muscleByPart/" + id, function(json) {

		//append the show all filter
		$( '#filters ul' ).append(
		'<li><a href="#" data-filter="*">Show all</a></li>'
		);

		// do an each on the json elements to loop through all the elements
		// "i" is the counter and "ex" is the JSON items
		$.each(json, function(i, ex) {

			//store all the muscles relevant
			filterMuscle = muscles[ex.id-1].slug;
			
			//append all relevany filters based on the body part
			$( '#filters ul' ).append(
				'<li><a href="#" data-filter=".' + filterMuscle + '">' + filterMuscle + '</a></li>'
			);

		});//each function
			
			//call the click function, so Isotope will register the new exercises
			$('#filters li a').click(function(){
				//declare the attribite
	 	  		var selector = $(this).attr('data-filter');
	 	  		//set a destination for the items
	 	  		var container = $('#exerciseList');
	 	  		//set the filter to the selector
	 	  		container.isotope({ filter: selector });
	 	  		//reload the items
	 	  		container.isotope( 'reloadItems' );
	 	  		return false;
	 		});

	});//FilterJSON function

	// get the json using the bodyPart
	$.getJSON("/demonstration/exerciseByPart/" + id, function(json) {
		// do an each on the json elements to loop through all the elements
		// "i" is the counter and "ex" is the JSON items
		$.each(json, function(i, ex) {

		//store all the muscle slugs related to the bodt part
		filterMuscle = muscles[ex.primary_muscle-1].slug; 
		
		//check to see whether there is a muscle for the exercise.
		if(ex.primary_muscle != 0) {	
			primaryMuscle = muscles[ex.primary_muscle-1].name;
		} else {
			//set muscle to blank string if not
			primaryMuscle = '';
		}
		
		//check to see whether there is a muscle for the exercise.
		if(ex.secondary_muscle != 0) {
			secondMuscle = muscles[ex.secondary_muscle-1].name;
		} else {
			//set muscle to blank string if not
			secondMuscle = '';
		}

		//check to see whether there is a muscle for the exercise.
		if(ex.third_muscle != 0) {
			thirdMuscle = muscles[ex.third_muscle-1].name;
		} else {
			//set muscle to blank string if not
			thirdMuscle = '';
		}	

		//check to see whether there is a muscle for the exercise.
		if(ex.fourth_muscle != 0) {
			fourthMuscle = muscles[ex.fourth_muscle-1].name;
		} else {
			//set muscle to blank string if not
			fourthMuscle = '';
		}

		// append to the data
		var $newItems = $(
			'<div class="exercises col' + ' ' + filterMuscle + '">' +
			'<div class="demonstration-video">' 
			+ '<iframe width="258" height="180" src="http://www.youtube.com/v/' + ex.demonstration_url + '"></iframe>' + 
			'</div>' +
			'<div class="exercise-title col">' + ex.name + '</div>' +
			'<div class="exercise-info"><h4>Primary Muscle Worked:</h4> ' + 
			primaryMuscle + '</div>' + 
			'<h4>Other Muscles Worked:</h4>' +
			'<div class="exercise-info">' + secondMuscle + '</div>' 
			+ '<div class="exercise-info">' + thirdMuscle + '</div>' +
			'<div class="exercise-info">' + fourthMuscle + '</div></div>'
		);

		//append the items to the div and to Isotope
		$('#exerciseList').append( $newItems ).isotope( 'addItems', $newItems );

		});//each function

	});//getJSON function
});//change function
</script>
</div>

@endsection