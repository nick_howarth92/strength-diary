@extends('app.master')

@section('main')

	<?php
		//get success message for the adding of a session from the controller
		$msg = Session::get('message');
		//if the message is not empty
		if (!empty($msg)) {
			//if the message is not empty pull it in, set a timeout for 3 seconds for the message to disapear
		   echo "<div class='profilepicmessage' id='msg'>" .$msg. "</div>
			<script>(function(id){
				window.setTimeout(function(el) {
					jQuery('#msg').hide();
				}, 3000);
			}());</script>";
		   //set to nothing when time has finished
		   Session::set('message', '');
		} 
	?>

	<div class="profile-info-container profile-margin">

		<?php 
			//get needed variables from session
			$userID = Auth::user()->id;
			$profilePicture = Auth::user()->profile_picture; 

		?>
				<div class="profile-pic-container">
					<div class="profile-picture">
						
						<!--if the profile picture isn't empty display it-->
						<?php if (!empty($profilePicture)): ?>
							<img src="uploads/<?php echo $userID ?>/<?php echo $profilePicture;?>" alt="user profile picture" />
						<?php endif; ?>

					</div>
					<div class="t-black">
						<small>
							<a href="editprofilepicture" rel="modal:open">
								Change Picture
							</a>
						</small>
					</div>
				</div>

				<div class="profile-name b-right">
					<h4>First Name:</h4>
					<p>{{Auth::user()->first_name}}</p>
				</div>
				<div class="profile-name">
					<h4>Last Name:</h4>
					<p>{{Auth::user()->second_name}}</p>
				</div>

				<div class="profile-email b-top">
					<h4>Email:</h4>
					<p>{{Auth::user()->email}}</p>
				</div>
				
					<?php
					 //store the session count in a variable
					 $total = count($sessions);
	                    //set the totalSession counter as zero
	                    $totalSessions = 0;
	                    //loop through all the total sessions
	                    for ($i = 0; $i < $total; $i++) {   

	                    	//add to the total              	
	                    	$totalSessions = $total;
	                    }
	                ?>

				<div class="profile-subcontainer b-right">
					<h4 class="t-center">Total Sessions</h4>
					<div class="total-session-container">
						<?php
						//check of there is any value
						if (empty($totalSessions)): 
						?>
						<div class="total-sessions green-bg">{{0}}</div>
					  	<?php else: ?>
						<div class="total-sessions green-bg">{{$totalSessions}}</div>
						<?php endif; ?>
					</div>
				</div>

				<div class="profile-subcontainer b-right">
					<h4 class="t-center">Achievements:</h4>
						<div class="trophy-container">
							<i class="entypo-trophy trophy"></i>
						</div>
						<p class="t-center">Coming Soon!</p>
				</div>

				<div class="profile-subcontainer b-right">
					<h4 class="t-center">BMI:</h4>
					<div class="bmi-container">
						<div class="bmi">11.9%</div>
					</div>
				</div>

				<?php 
					//get the date joined of the user
					$dateJoined = Auth::user()->created_at; 
					$dateJoined = date("Y-m-d", strtotime($dateJoined));
				?>

				<div class="profile-subcontainer">
					<h4 class="t-center">Member Since:</h4>
					<div class="memberfrom-container"> 
						<div class="memberfrom">
						<div class="memberdate t-white">{{$dateJoined}}</div>
						</div>
					</div>
				</div>

		</div><!--profile-info-container-->
		
		<br class="clear" />

		<div class="container profile-margin">

			<h2>Recent Activity</h2>

			@foreach ($sessions as $session)
					<?php 	
							//get the notes and the date it was created
							$recentNotes = $session->notes;
							$recentDate = $session->created_at;
							//convert the date
							$recentDate = date("d - M", strtotime($recentDate));
					?>
				<!--if there are notes on the activity-->
				@if(!empty($recentNotes))
					<div class="recent-activity-container">
						<div class="recent-activity-date">{{$recentDate}}</div>
						<div class="recent-activity-text">{{$recentNotes}}</div>
					</div>
				@endif

			@endforeach

@endsection