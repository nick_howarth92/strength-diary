<?php 
//get the required data and store in variables
$id = $exercisePlannedID->id;
$name = $exercisePlannedID->exercise->name;

?>
<h2 class="t-center">{{ $name }}</h2>

<div class="full-linebreak"></div><br>

<form method="post" id="scoreForm" action="addscore" onclick="return scoreForm()">

	<h4 class="t-center t-black">Effort level of exercise</h4>

	<input type="hidden" name="exerciseID" value="{{$id}}" />

	<div class="effort-center">
		
		<div class="c-left effort-caption">Easy</div>

		<div class="c-left">
			<input type="radio" name="effort" value="5" checked />
		</div>
		
		<div class="c-left">
			<input type="radio" name="effort" value="4" />
		</div>

		<div class="c-left">
			<input type="radio" name="effort" value="3" />
		</div>

		<div class="c-left">
			<input type="radio" name="effort" value="2" />
		</div>

		<div class="c-left">
			<input type="radio" name="effort" value="1" />
		</div>

		<div class="c-left effort-caption">Challenging</div>

	</div><!--effort-score-->

	<br class="clear" /><br class="clear" />
		
		<div class="t-center t-black">
				<a id="addSet" href="javascript:void(0)">
					<div>Add a set &nbsp;
						<i class="entypo-list-add add-icon"></i>
					</div>
				</a>
		</div>
	
	<br class="clear"/>

	<div id="setList">

	</div>

	<div class="s-center">	
		<input class="m-top" type="submit" value="Add Score!" />
	</div>

</form>
<script>

//create an onclick event to populate sets
$("#addSet").click(function () {
		//append a set to the form
		$('#setList').append(
			'<span class="errors" id="weightCheck"></span>' +
			'<div class="score-center">' + 
			'<input type="text" name="weight[]" class="score-input col" placeholder="weight..." required />' + 
			'<input type="text" name="reps[]" class="score-input col" placeholder="reps..." required/>' + '<div id="removeSet">' + 
			'<a class="removeRow" href="javascript:void(0);">Remove</a>' + '</div></div>'
		);
}); //on click

//click function to delete row in selection
$("#setList").delegate(".removeRow", "click", function() {
	$(this).parent().parent().remove();
});

</script>