@extends('app.master')

@section('main')

	<div class="container profile-margin margin-top">
	
	<div class="progression-tabs">
		
		<!--tabs-->
		<ul class="tabs">
            <li><a href="#overall">Overall</a></li>
            <li><a href="#strength">Strenth Summary</a></li>
            <li><a href="#endurance">Endurance Summary</a></li>
   		</ul><!--tabs-->

		<div class="tabs-container">
			
			<!--overall progression section-->
			<div id="overall">
				<div class="tabs-col col">

					<h3 class="t-center">Overall Progression by Entry</h3>
					<!--test if there are any entries, if there is display the graph-->
					@if(sizeof($entries) > 0)
						
						<div class="linechart-holder">
							<canvas id="overallLineChart" width="500" height="352"></canvas>
						</div>
				
						<script>
						//line chart for overall progression
						var overallLineChartData = {
							labels :  ["1","2","3","4","5","6","7","8","9","10"],
							datasets : [
								{
									fillColor : "rgba(220,220,220,0.5)",
									strokeColor : "rgba(220,220,220,1)",
									pointColor : "rgba(220,220,220,1)",
									pointStrokeColor : "#fff",
									data : [
									<?php 
										foreach ($entries as $entry) {
											//grab the types
											if($entry->exercisePlanned):
												//if the type is = 1
												if($entry->exercisePlanned->type == 1):
													//add to the strength counter 
													echo $entry->score . ", ";
												endif;
											endif;
										}
									?>
									]//end data for strength
								},
								{
									fillColor : "rgba(151,187,205,0.5)",
									strokeColor : "rgba(151,187,205,1)",
									pointColor : "rgba(151,187,205,1)",
									pointStrokeColor : "#fff",
									data : [
									<?php 
										foreach ($entries as $entry) {
											
												//grab the types
												if($entry->exercisePlanned):
													//if the type is = 1
													if($entry->exercisePlanned->type == 2):
														//add to the strength counter 
														echo $entry->score . ", ";
													endif;
												endif;
										}
									?>
									]//end data for endurance
								}
							]
							
						}
						</script>

					
						<!--key for line chart-->
						<div class="key-container">
							<div class="line-key">
								<img src="{{ asset('images/progression/strength-line2.png') }}" />
							</div>
							<div class="line-key">
								<img src="{{ asset('images/progression/endurance-line2.png') }}" />
							</div>
						</div>

						<script>
						//create a new line chart for overall progression in strength and endurance
						var overallLine = new Chart(document.getElementById("overallLineChart").getContext("2d")).Line(overallLineChartData);
						</script>
					@else
						<div class="nograph">Woops - No data added yet!</div>
					@endif
				
				</div><!--tabs-col-->

				<div class="tabs-col">
				
					<h3 class="t-center">Session Totals</h3>
					
					<!--test if there are any entries, if there is display the graph-->
					@if(sizeof($entries) > 0)
					
						<div class="piechart-holder">
							<canvas id="overallPieChart" width="350" height="350"></canvas>
						</div>


						<?php 
						//set the counters for session types
						$strengthCounter = 0; 
						$enduranceCounter = 0;
						?>
						
						<!--loop around all the entries-->
						@foreach ($entries as $entry)
							<?php
								//grab the types
								if($entry->exercisePlanned):
									//if the type is = 1
									if($entry->exercisePlanned->type == 1):
										//add to the strength counter 
										$strengthCounter++;
									endif;

									//if the type is = 2
									if($entry->exercisePlanned->type == 2):
										//add to the endurance counter
										$enduranceCounter++;
									endif;
								
								endif;
							?>
						@endforeach
						
						<script>
						//pie chart
						var overallPieData = [
							{
								value: <?php echo $strengthCounter; ?>,
								color:"#69D2E7"
							},
							
							{
								value : <?php echo $enduranceCounter; ?>,
								color : "#F38630"
							}			
						]
						</script>
					
						<!--key for Pie chart-->
						<div class="key-container">
							<div class="pie-key">
								<img src="{{ asset('images/progression/strength-pie.png') }}" />
							</div>
							<div class="pie-key">
								<img src="{{ asset('images/progression/endurance-pie.png') }}" />
							</div>
						</div>
						<script>
						//create a new pie chart for the different in sessions
						var overallPie = new Chart(document.getElementById("overallPieChart").getContext("2d")).Pie(overallPieData);
						</script>
					@else
						<div class="nograph">Woops - No data added yet!</div>
					@endif

				</div><!--tabs-col-->
				
				<?php
				 //store the session count in a variable
				 $seshTotal = count($sessions);
                    //set the totalSession counter as zero
                    $totalSessions = 0;
                    //loop through all the total sessions
                    for ($i = 0; $i < $seshTotal; $i++) {   

                    	//add to the total              	
                    	$totalSessions = $seshTotal;
                    }
                ?>	

				<div class="statistics-container">
					<div class="statistics-item red-bg">
						<h3 class="t-white statistic-text">
						<?php 
						//check of there is any value
						if (empty($totalSessions)): 
							//display 0 if empty
							echo "0";
					  	else:
					  		//display totals
					  		echo $totalSessions;
					  	endif;
						?>

						</h3>
						<p class="t-white">Total Sessions</p>
						<i class="entypo-clipboard t-white"></i>
					</div>

					<div class="statistics-item light-purple-bg">
						<h3 class="t-white statistic-text">
						<?php  
						//check of there is any value
						if (empty($highestEntry->score)):
							//display zero if empty
							echo "0";
					    else:
					    	//display totals
					   		echo $highestEntry->score;
					    endif; 
						?>
						</h3>
						<p class="t-white">Highest Score</p>
						<i class="entypo-chart-bar t-white"></i>
					</div>

					<?php 
					//set the totals to zero
					$totalWeight = 0; 
					$totalReps = 0; 
					?>
	                
	                <!--loop around all the entries-->
	                @foreach ($entries as $entry)
						<?php
							//add to the totals
							$totalReps += $entry->total_reps;
							$totalWeight += $entry->total_weight;
	                	?>
	                @endforeach

					<div class="statistics-item purple-bg">
						<h3 class="t-white statistic-text">
						<?php
						//check of there is any value
						if (empty($totalReps)): 
							//display a 0 if empty
							echo "0";
					 	else:
					 	 	//display total if not empty
					  		echo $totalReps;
					 	endif;
						?>
						</h3>
						<p class="t-white">Total Repetitons</p>
						<i class="entypo-volume t-white"></i>
					</div>

					<div class="statistics-item light-green-bg">
						<h3 class="t-white statistic-text">
						<?php
						//check of there is any value
						if (empty($totalWeight)): 
							//display 0 if empty
							echo "0";
						else:
							//display totals
						  	echo $totalWeight .' '. "kg";
						endif;	
						?>
						</h3>
						<p class="t-white">Total Weight Lifted</p>
						<i class="entypo-gauge t-white"></i>
					</div>
				</div><!--statistics-container-->

			</div><!--overall-tabs-->

			<div id="strength">
				
				<div class="compound-col col">
					<h3>Check your Compound exercises</h3>
					<div class="progressionFilter-holder col">
						<select id="compoundExercises" name="compoundExercises">
							<option value="">Select Compound Exercise</option>
							<!--loop around all the compound exercises-->

								@foreach($compoundExercises as $compoundExercise)
									<option id="compound" value="{{ $compoundExercise->id }}">
										<!--display the compound exercises-->
										{{ $compoundExercise->name }}
									</option>
								@endforeach
						</select>
					</div>

					<!--div for graph error message-->
					<div id="graphError"></div>
				
					<div id="compound-LineChart">
						 <canvas id="compoundLineChart" width="450" height="352"></canvas>
					</div>
				</div><!--tabs-col-->

				<div class="filter-tabs-col">
			    	<h3>Find an exercise to view your progression.</h3>
					
					<div class="progressionFilter-holder col">
						<select id="stBodyPart" name="stBodyPart">
						<option value="">1. Select Body Part</option>
						<!--loop around all the body exercises-->
							@foreach($bodyParts as $bodyPart)
								<option value="{{ $bodyPart->id }}">
									<!--display the body parts-->
									{{ $bodyPart->name }}
								</option>
							@endforeach
						</select>
					</div>
					
					<div class="progressionFilter-holder col">
						<select id="stMuscleType" name="stMuscleType">
							<option value="">2. Select Muscle Type</option>
						</select>
					</div>
					
					<div class="progressionFilter-holder col">
						<select class="select-exercises" id="stExerciseType" name="stExerciseType">
							<option value="">3. Select Exercise</option>	
						</select>
					</div>

					<div id="strength-lineChart">
						<canvas id="strengthLineChart" width="450" height="352"></canvas>
					</div>

				</div><!--tabs-col-->

			</div><!--tabs-2-->
			
			<!--endurance progression-->
			<div id="endurance">

			    <h3>Find an exercise to view your progression.</h3>
					<div class="progressionFilter-holder col">
						
						<select id="enBodyPart" name="enBodyPart">
						<option value="">1. Select Body Part</option>
						<!--loop around the body parts-->
							@foreach($bodyParts as $bodyPart)
								<option value="{{ $bodyPart->id }}">
									<!--display them-->
									{{ $bodyPart->name }}
								</option>
							@endforeach
						</select>
					</div>
					
					<div class="progressionFilter-holder col">
						<select id="enMuscleType" name="enMuscleType">
							<option value="">2. Select Muscle Type</option>
						</select>
					</div>
					
					<div class="progressionFilter-holder col">
						<select class="select-exercises" id="enExerciseType" name="enExerciseType">
							<option value="">3. Select Exercise</option>	
						</select>
					</div>

					<div id="endurance-lineChart">
						<canvas id="enduranceLineChart" width="450" height="352"></canvas>
					</div>

			    
			</div><!--tabs-3-->

		</div><!--tabs-container-->	
	</div><!--progression-tabs-->
</div><!--main-container-->

<script>

//detect on change of the select box
$('#stBodyPart, #enBodyPart').change(function() {

//remove anything in the other select boxes if there is any, strength
$('#stMuscleType #stMuscleOptions ').remove();
$('#stExerciseType #stExerciseOptions ').remove();

//remove anything in the other select boxes if there is any, endurance
$('#enMuscleType #enMuscleOptions ').remove();
$('#enExerciseType #enExerciseOptions ').remove();

// get value of the body part from the select box	
var stId = $('#stBodyPart').val(),
	enId = $('#enBodyPart').val(),
	muscles = <?php echo $muscles; ?>,
	filterMuscle;

	//getJSON function for strength
	// get the filtered Primary Muscle using the bodyPart
	$.getJSON("/progression/muscleByPart/" + stId, function(json) {

		// do an each on the json elements to loop through all the elements
		// "i" is the counter and "ex" is the JSON items
		$.each(json, function(i, ex) {
			//store the muscle names
			filterMuscle = muscles[ex.id-1].name;
				//append the muscles to the select box
				$( '#stMuscleType' ).append(
					'<option id="stMuscleOptions" value="' + muscles[ex.id-1].id + '">' + filterMuscle + '</option>'
				);

		});//each function
	});//FilterJSON function

	//getJSON function for endurance
	//get the filtered Primary Muscle using the bodyPart
	$.getJSON("/progression/muscleByPart/" + enId, function(json) {

		// do an each on the json elements to loop through all the elements
		// "i" is the counter and "ex" is the JSON items
		$.each(json, function(i, ex) {
			//store the muscle names
			filterMuscle = muscles[ex.id-1].name;
				//append the muscles to the select box
				$( '#enMuscleType' ).append(
					'<option id="enMuscleOptions" value="' + muscles[ex.id-1].id + '">' + filterMuscle + '</option>'
				);

		});//each function
	});//FilterJSON function

});//change function

//detect on change of the Muscle select box
$('#stMuscleType, #enMuscleType').change(function() {

//remove any exercise if there is any
$('#stExerciseType #stExerciseOptions ').remove();
$('#enExerciseType #enExerciseOptions ').remove();

// get value of the muscle id from the select box	
var stId = $('#stMuscleType').val(),
	enId = $('#enMuscleType').val(),
	//store an array of exercises
	exercises = <?php echo $exercises; ?>,
	filterExercise,
	exerciseID;

		//getJSON function for strength
		//get the exercises of the muscle in selection
		$.getJSON("/progression/exerciseByPart/" + stId, function(json) {
			// do an each on the json elements to loop through all the elements
			// "i" is the counter and "ex" is the JSON items
			$.each(json, function(i, ex) {
				//store the exercises
				filterExercise = exercises[ex.id-1].name;
				exerciseID = exercises[ex.id-1].id;
					//append the exercises to the select box
					$( '#stExerciseType' ).append(
						'<option id="stExerciseOptions" value="' +exerciseID+ '">' + filterExercise + '</option>'
					);

			});//each function
		});//FilterJSON function

		//getJSON function for endurance
		//get the exercises of the muscle in selection
		$.getJSON("/progression/exerciseByPart/" + enId, function(json) {

			// do an each on the json elements to loop through all the elements
			// "i" is the counter and "ex" is the JSON items
			$.each(json, function(i, ex) {
				//store the exercises
				filterExercise = exercises[ex.id-1].name;
				exerciseID = exercises[ex.id-1].id;
					//append the exercises to the select box
					$( '#enExerciseType' ).append(
						'<option id="enExerciseOptions" value="' +exerciseID+ '">' + filterExercise + '</option>'
					);

			});//each function
		});//FilterJSON function

});//change function

//detect on change of the exercise select box
$('#stExerciseType').change(function() {
	//declare an empty array
	var dataValues = [];
	var stID = $('#stExerciseType').val();

	$('#strengthLineChart').remove();

		//getJSON function for endurance
		//get the exercises of the muscle in selection
		$.getJSON("/progression/exerciseScore/" + stID +'/type/1', function(json) {
			
			//set all the data labels
			var dateLabels = ["1","2","3","4","5","6","7","8","9","10"];
			
			//iterate through all the scores with the exercise id
			$.each(json, function(i, ex) {
				//push to the array all the scores
				dataValues.push(parseFloat(ex.score));

			});//each
			
			// add a value for each label
			if (dataValues.length < dateLabels.length) {
				//loop through all the dataLabels and set to 0 if there is no dataValue
				for (var n=dataValues.length; n < dateLabels.length; n++) {
					dataValues.push(0);
				}
			}

			//append the canvas for chart
			$('#strength-lineChart').append(
				'<canvas id="strengthLineChart" width="450" height="352"></canvas>'
			);


			//strength line data
			var strengthLineChartData = {
				labels : dateLabels,
				datasets : [
					{
						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,1)",
						pointColor : "rgba(151,187,205,1)",
						pointStrokeColor : "#fff",
						data : dataValues

					}
				]
				
			}//graph

			//endurance line chart
			var strengthLine = new Chart(document.getElementById("strengthLineChart").getContext("2d")).Line(strengthLineChartData);

		});//FilterJSON function

});//change function

//detect on change of the exercise select box
$('#enExerciseType').change(function() {

	//declare an empty array
	var dataValues = [];
	var enID = $('#enExerciseType').val();

	$('#enduranceLineChart').remove();

		//getJSON function for endurance
		//get the exercises of the muscle in selection
		$.getJSON("/progression/exerciseScore/" + enID +'/type/2', function(json) {
			
			//set all the data labels
			var dateLabels = ["1","2","3","4","5","6","7","8","9","10"];
			
			//iterate through all the scores with the exercise id
			$.each(json, function(i, ex) {
				//push to the array all the scores
				dataValues.push(parseFloat(ex.score));

			});//each
			
			// add a value for each label
			if (dataValues.length < dateLabels.length) {
				//loop through all the dataLabels and set to 0 if there is no dataValue
				for (var n=dataValues.length; n < dateLabels.length; n++) {
					dataValues.push(0);
				}
			}

			//append the canvas for chart
			$('#endurance-lineChart').append(
				'<canvas id="enduranceLineChart" width="450" height="352"></canvas>'
			);


			//endurance line data
			var enduranceLineChartData = {
				labels : ["1","2","3","4","5","6","7","8","9","10"],
				datasets : [
					{
						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,1)",
						pointColor : "rgba(151,187,205,1)",
						pointStrokeColor : "#fff",
						data : dataValues
					}
				]
				
			}
			//strength line chart
			var enduranceLine = new Chart(document.getElementById("enduranceLineChart").getContext("2d")).Line(enduranceLineChartData);

		});//FilterJSON function

});//change function

//detect on change of the compound muscle select box
$('#compoundExercises').change(function() {

	//declare an empty array
	var dataValues = [];
	var cID = $('#compoundExercises').val();

	//remove any contents needed
	$('#compoundLineChart').remove();
	$('#graphError div').remove();

		//getJSON function for endurance
		//get the exercises of the muscle in selection
		$.getJSON("/progression/exerciseScore/" + cID +'/type/1', function(json) {
			
			//set all the data labels
			var dateLabels = ["1","2","3","4","5","6","7","8","9","10"];
			
			// iterate through all the scores with the exercise id
			$.each(json, function(i, ex) {
				//push to the array all the scores
				dataValues.push(parseFloat(ex.score));
			});//each
		
			// add a value for each label
			if (dataValues.length < dateLabels.length) {
				//loop through all the dataLabels and set to 0 if there is no dataValue
				for (var n=dataValues.length; n < dateLabels.length; n++) {
					dataValues.push(0);
				}
			}
			
			//append the graph for compound exercise score
			$('#compound-LineChart').append(
				'<canvas id="compoundLineChart" width="450" height="352"></canvas>'
			);

			//compound line data
			var compoundLineChartData = {
				labels : ["1","2","3","4","5","6","7","8","9","10"],
				datasets : [
					{
						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,1)",
						pointColor : "rgba(151,187,205,1)",
						pointStrokeColor : "#fff",
						data : dataValues
					}
				]
				
			}

			//strength line chart
			var compoundLine = new Chart(document.getElementById("compoundLineChart").getContext("2d")).Line(compoundLineChartData);
		
		}).fail(function() {
			$('#graphError').append (
				'<div class="nograph">No Scores for this exercise yet</div>' 
			);
		});//FilterJSON function

});//change function

</script>
	
@endsection