<!doctype html>
<html>
<head>
	<title>Strength Diary | Profile </title>

	<!--Main Stylesheets-->
	{{ HTML::style('css/normalize.css') }}
	{{ HTML::style('css/global.css') }}
	{{ HTML::style('css/app.css') }}
	<!--tabs-->
	{{ HTML::style('css/tabs/tabular.css') }}

	<!--modal-->
	{{ HTML::style('css/modal/modal.css') }}
	
	<!--google fonts-->
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,700' rel='stylesheet' type='text/css' />

	<!--date picker styles-->
	<link rel="stylesheet" href="{{ asset('css/datepicker/default.css') }}" id='theme_base'>
	<link rel="stylesheet" href="{{ asset('css/datepicker/default.date.css') }}" id="theme_date">
	<link rel="stylesheet" href="{{ asset('css/datepicker/default.time.css') }}" id="theme_time">

	<!--calendar-->
	{{ HTML::style('css/calendar/c.css') }}
	<!--isotoper filtering css-->
	{{ HTML::style('css/isotope/filter.css') }}

	<!--Modernizer JS fix for IE 8-->
	{{ HTML::script('js/modernizer/modernizer.js') }}
	<!--Media queries IE 8-->
	{{ HTML::script('js/css3mediaqueries/css3-mediaqueries.js') }}

	<!--js libray-->
	{{ HTML::script('http://code.jquery.com/jquery-1.10.2.min.js') }}

	<!--progression JS line chart-->
	{{ HTML::script('js/charts/Chart.js') }} 

</head>

<body>

<div class="nav-container">	
		<nav>
			<ul>
				<li class="current">
					<a class="logo" href="{{ url('profile') }}">
						<img src="{{ asset('images/logo-profile.png') }}" alt="Strength Diary Logo" />
					</a>
				</li>
				<li>
					<a href="{{ url('schedule') }}"><i class="fontawesome-calendar schedule"></i>
						<span>Schedule</span>
					</a>
				</li>
				<li><a href="{{ url('progression') }}"><i class="typicons-lineChart progress"></i>
						<span>Progression</span>
					</a>
				</li>
			
				<li><a href="{{ url('demonstration') }}">
				<i class="entypo-video demonstrations"></i>
						<span>Demonstrations</span>
					</a>
				</li>
			</ul>
		</nav>
			<div class="logout-container">
				<a href="{{ url('logout') }}">
					<i class="entypo-logout logout"></i></a><br />
				<span class="logout-text">Logout</span>
			</div>
</div><!--nav-container-->

	<div>
		@yield('main')
	</div>

<!--date picker JS-->
{{ HTML::script('js/datepicker/picker.js') }} 
{{ HTML::script('js/datepicker/picker.date.js') }} 
{{ HTML::script('js/datepicker/legacy.js') }} 

<!--Modal JS-->
{{ HTML::script('js/modal/reveal.js') }}

<!--big calendar JS-->
{{ HTML::script('js/calendar/calendar.js') }} 

<!--Isotope Filtering JS-->
{{ HTML::script('js/isotope/jquery.isotope.js') }}
{{ HTML::script('js/isotope/jquery.isotope.min.js') }} 

<!--Main JS-->
{{ HTML::script('js/main.js') }} 

</body>
</html>