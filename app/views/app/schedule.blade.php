@extends('app.master')

@section('main')

	<?php

		//get success message for the adding of a session from the controller
		$msg = Session::get('addmessage');
		//if the message is not empty pull it in, set a timeout for 3 seconds for the message to disapear
		if (!empty($msg)) {
		   echo "<div class='addmessage' id='msg'>" .$msg. "</div>
			<script>(function(id){
				window.setTimeout(function(el) {
					jQuery('#msg').hide();
				}, 3000);
			}());</script>";
		   //set to nothing when time has finished
		   Session::set('addmessage', '');
		} 

		//get success message for the adding of a score from the controller
		$score = Session::get('scoremessage');
		//if the messafe is not empty pull it in, set a timeout for 3 seconds for the message to disapear
		if (!empty($score)) {
				echo "<div class='scoremessage' id='score'>" .$score. "</div>
				<script>(function(id){
					window.setTimeout(function(el) {
						jQuery('#score').hide();
					}, 3000);
				}());</script>";
				//set to nothing when time has finished
			   Session::set('scoremessage', '');
			}

		//get error message for the adding of a weight and rep from the controller
		$msg = Session::get('wrerror');
		//if the message is not empty pull it in, set a timeout for 3 seconds for the message to disapear
		if (!empty($msg)) {
		   echo "<div class='wr-errormessage' id='msg'>" .$msg. "</div>
			<script>(function(id){
				window.setTimeout(function(el) {
					jQuery('#msg').hide();
				}, 5000);
			}());</script>";
		   //set to nothing when time has finished
		   Session::set('wrerror', '');
		} 

		//get error message for the adding of a score from the controller
		$msg = Session::get('scorerror');
		//if the message is not empty pull it in, set a timeout for 3 seconds for the message to disapear
		if (!empty($msg)) {
		   echo "<div class='score-errormessage' id='msg'>" .$msg. "</div>
			<script>(function(id){
				window.setTimeout(function(el) {
					jQuery('#msg').hide();
				}, 5000);
			}());</script>";
		   //set to nothing when time has finished
		   Session::set('scorerror', '');
		} 
	?>

	<div class="container profile-margin">

		<h2 class="t-center margin-top">Add a Session</h2>

		<div class="addsession-container">

			<form method="post" id="scheduleForm" action="addsession" onsubmit="return scheduleForm()">
			<div class="sessiontype-container">
					
					<div class="c-left">
						<h4 class="col">The type of training?</h4>
					</div>

					<div class="c-left">
						<div class="addsession-holder workout-type-radio col">
							Strength<input type="radio" name="activity-type" value="1" checked />
						</div>
					</div>
					
					<div class="c-left">
						<div class="addsession-holder workout-type-radio">
							Endurance<input type="radio" name="activity-type" value="2" />
						</div>
					</div>
			</div><!--sessiontype-container-->
				
				<!--pull in errors from controller-->
				<div class="c-left errors e-mbot">
					{{ $errors->first('sessionname'); }}
				</div>

				<!--pull in erorrs using javaScript-->
				<span class="errors" id="sessionnameCheck"></span>
				
				<input type="text" name="sessionname" class="fullwidth-input e-mbot" placeholder="Session Name..." required /><br>
				
				<!--pull in errors from controller-->
				<div class="c-left errors ">
					 {{ $errors->first('date'); }} 
				</div>
				
				<!--pull in erorrs using javaScript-->
				<span class="errors" id="dateCheck"></span>

				<!--date picker-->
				<div class="datepicker-holder col">
					<input class="datepicker" type="text" name="date" placeholder="Pick a Date..." required="required" />
				</div>

				<input type="text" name="notes" class="notes" placeholder="Notes..." />
				
				<br class="clear" />

				<div class="addsession-exercise-holder">
					
					<div class="exercise-select-holder col">
						<select id="bodyPart" name="bodyPart">
						<option value="">1. Select Body Part</option>
						<!--loop around all the body parts-->
							@foreach($bodyParts as $bodyPart)
							<!--pull body parts into options-->
								<option value="{{ $bodyPart->id }}">
									{{ $bodyPart->name }}
								</option>
							@endforeach
						</select>
					</div>
					
					<div class="exercise-select-holder col">
						<select id="muscleType" name="muscleType">
							<option value="">2. Select Muscle Type</option>
						</select>
					</div>
					
					<div class="exercise-select-holder">
						<select class="select-exercises" id="exerciseType" name="exerciseType">
							<option value="">3. Select Exercise</option>	
						</select>
					</div>

					<div id="showVideo" class="add-session-link">

					</div>

					<div class="add-session-link">
						<a id="addExercise" href="javascript:void(0)">
							<i class="entypo-list-add add-icon"></i>
						</a>
					</div>

					<br class="clear">

					<!--pull in errors from controller-->
					<div class="c-left errors ">
						{{ $errors->first('weight'); }}
					</div>	
					
					<!--exercise append to this div-->
					<div id="scheduledExercises">
					<h4 class="t-center">Exercise List:</h4>
					<div class="thin-line"></div>
						<!--pull in errors from controller-->
						<div class="c-left errors ">
							 {{ $errors->first('exerciseID'); }}
						</div>	
					</div>


				</div><!--addsession-exercise-holder-->
				<br class="clear"/><br class="clear"/>
				
				<input class="c-left" type="submit" value="Add Session" />
				
			</form><!--end schedule form-->

		</div><!--addsession-container-->

		<h2 class="t-center">Scheduled Workouts</h2>
		
		<div class="left googlecal">
				<a href="exportGoogleCal">Export Session to Google Calendar</a>
		</div>

		<div class="calendar" data-showdays="true" data-color="green" style="height:20em">
			<!--looping round all the sessions-->
			@foreach ($sessions as $session)
				<?php  
					//pull in needed data, store in variables
					$sessionID = $session->id;
				 	$sessionTitle = $session->title;
				 	$sessionNotes =$session->notes;
				 	$sessionDate = $session->scheduled_for;

				 	//convert to appropiate format for calendar
				 	$sessionDate = date('Ymd', strtotime($sessionDate));

				?>

				<div data-role="day" data-day="{{$sessionDate}}">
					<!--loop around the exercises planned-->
		     		@foreach ($exercisesPlanned as $exercisePlanned)
					
					<?php 
						//pull in needed data, store in variables
						$exerciseSessionID = $exercisePlanned->session_id;
						$exercisePlannedID = $exercisePlanned->id;
						$exerciseList = $exercisePlanned->exercise->name; 
					?>

						<!--The sessionID is equal to the exerciseSessionID-->
						@if ($sessionID == $exerciseSessionID)
				     		<!--pull in details of session-->
				     		<div 
				     			data-role="event" data-id="{{$exercisePlannedID}}" 
				     			data-name="{{$sessionTitle}}"
				     			data-notes="{{$sessionNotes}}" data-end=""
				     			data-exercise="{{$exerciseList}}">
				     		</div>

						@endif

		     		@endforeach

				</div><!--session day(data-role="day")-->
				
	     	@endforeach<!--main for loop for sessions in calendar-->
	  		
		</div><!--calendar-->

	</div><!--container-->

<script>
//declare an object and if one doesn't currently exsist set it to a blank object
var NICK = NICK || {};

//detect on change of the body part select box
$('#bodyPart').change(function() {
	//remove anything in the other select boxes if there is any, strength
	$('#muscleType #muscleOptions ').remove();
	$('#exerciseType #exerciseOptions ').remove();
	//remove current video from the link
	$('#showVideo a').remove();

	// get value of the body part from the select box	
	var id = $('#bodyPart').val(),
		muscles = <?php echo $muscles; ?>,
		filterMuscle;

		//getJSON function for strength
		// get the filtered Primary Muscle using the bodyPart
		$.getJSON("/schedule/muscleByPart/" + id, function(json) {

			// do an each on the json elements to loop through all the elements
			// "i" is the counter and "ex" is the JSON items
			$.each(json, function(i, ex) {
				//store the muscle names
				filterMuscle = muscles[ex.id-1].name;
					//append the muscles to the select box
					$( '#muscleType' ).append(
						'<option id="muscleOptions" value="' + muscles[ex.id-1].id + '">' + filterMuscle + '</option>'
					);

			});//each function
		});//FilterJSON function

});//change function

//detect on change of the muscle select box
$('#muscleType').change(function() {
	//make the nick object equal a chosen exercise
	NICK.chosenExercises = {};
	//remove any exercise if there is any
	$('#exerciseType #exerciseOptions ').remove();
	//remove current video from the link
	$('#showVideo a').remove();


	// get value of the body part from the select box	
	var id = $('#muscleType').val(),
		exercises = <?php echo $exercises; ?>,
		filterMuscle;

		//getJSON function for strength
		// get the filtered Primary Muscle using the bodyPart
		$.getJSON("/schedule/exerciseByPart/" + id, function(json) {

			// do an each on the json elements to loop through all the elements
			// "i" is the counter and "ex" is the JSON items
			$.each(json, function(i, ex) {
				//store the muscle names
				filterMuscle = exercises[ex.id-1].name;
					//append the exercises to the select box
					$( '#exerciseType' ).append(
						'<option id="exerciseOptions" value="' + exercises[ex.id-1].id + '">' + filterMuscle + '</option>'
					);

			});//each function
		});//FilterJSON function

});//change function

//detect on change of the exercise select box
$('#exerciseType').change(function() {
	// get value of the exercise from the select box	
	var exerciseID = $('#exerciseType').val(),
	//pull in the indivdual exercise array
	indivExercises = <?php echo $indivExercises; ?>,
	exerciseName = "";

	//remove current video from the link
	$('#showVideo a').remove();

	//getJSON function for strength
	// get the filtered Primary Muscle using the bodyPart
	$.getJSON("/schedule/indivdualExercise/" + exerciseID, function(json) {
		// do an each on the json elements to loop through all the elements
		// "i" is the counter and "ex" is the JSON items
		$.each(json, function(i, ex) {
			//store the exercise names
			exerciseName = indivExercises[ex.id-1].name;

		});//each function

	});//FilterJSON function

	//append the info icon with exercise ID attached
	$('#showVideo').append(
		'<a class="info-icon" href="showvideo/' + exerciseID + '" rel="modal:open">' +
			'<i class="entypo-info"></i></a>'
	);

});//change function
	
//create an onclick event to populate scheduled workouts from select box
$( "#addExercise" ).on("click", function () {
	//declare a variable to get the value of the exercise type
	var currExVal = $('#exerciseType').val(),
		//create another variable to get the text from the current exercise option selected in select box
		currExTxt = $('#exerciseType option:selected').text();

	//if there is an exercise selected and the exercise doensn't exsist
	if (currExVal && !NICK.chosenExercises[currExVal]) {
		NICK.chosenExercises[currExVal] = currExVal;
		//append the exercises to the div
		$('#scheduledExercises').append(
			'<div class="indiv-exercise">' +
			//pass through the exercise ID as an array and set it to hidden 
			'<input type="hidden" name="exerciseID[]" value="'+ currExVal +'" />' +
			//have the exercise name appear in the list for the user
			'<input class="exercise-name-list" value="Exercise Name:'+'   '+ currExTxt +'" />' + '<a class="remove-exercise" href="javascript:void(0);"><i class="entypo-cancel-squared"></i></a>' + '</div>'
		);

	}//endif
}); //on click

//click function to delete exercise in selection
$("#scheduledExercises").delegate(".remove-exercise", "click", function() {
	$(this).parent().remove();
});

</script>

@endsection