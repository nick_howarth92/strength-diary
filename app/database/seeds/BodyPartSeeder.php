<?php

class BodyPartSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('body_part')->delete();

		BodyPart::create(array(

			'id' => '',
			'name' => 'Back',
			'created_at' => new DateTime,
            'updated_at' => new DateTime
		));

		BodyPart::create(array(

			'id' => '',
			'name' => 'Chest',
			'created_at' => new DateTime,
            'updated_at' => new DateTime
		));

		BodyPart::create(array(

			'id' => '',
			'name' => 'Shoulders',
			'created_at' => new DateTime,
	        'updated_at' => new DateTime
		));

		BodyPart::create(array(

			'id' => '',
			'name' => 'Arms',
			'created_at' => new DateTime,
            'updated_at' => new DateTime
		));

		BodyPart::create(array(

			'id' => '',
			'name' => 'Abdominals',
			'created_at' => new DateTime,
            'updated_at' => new DateTime
		));

		BodyPart::create(array(

			'id' => '',
			'name' => 'Legs',
			'created_at' => new DateTime,
            'updated_at' => new DateTime
		));

	}

}