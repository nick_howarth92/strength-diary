<?php

class ExerciseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('exercises')->delete();

		//BACK EXERCISE----------------------------
		//Traps
		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Face Pull',
		'primary_muscle' => '1',
		'secondary_muscle' => '6',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'tkLTR4b6cAk',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Barbell Shrug',
		'primary_muscle' => '1',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'tkLTR4b6cAk',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Dumbbell Shrug',
		'primary_muscle' => '1',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => '8lP_eJvClSA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//Middle Back
		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Bent Over Barbell Row',
		'primary_muscle' => '2',
		'secondary_muscle' => '7',
		'third_muscle' => '3',
		'fourth_muscle' => '6',
		'demonstration_url' => '-xlBxIMqh3A',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Seated Cable Rows',
		'primary_muscle' => '2',
		'secondary_muscle' => '7',
		'third_muscle' => '3',
		'fourth_muscle' => '6',
		'demonstration_url' => 'IzoCF_b3cIY',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'One Arm Dumbbell Row',
		'primary_muscle' => '2',
		'secondary_muscle' => '7',
		'third_muscle' => '3',
		'fourth_muscle' => '6',
		'demonstration_url' => 'PgpQ4-jHiq4',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Bent Over Two Arm Long Bar Row',
		'primary_muscle' => '2',
		'secondary_muscle' => '7',
		'third_muscle' => '3',
		'fourth_muscle' => '6',
		'demonstration_url' => 'e8YF8Z_N0-4',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Bent Over Two Dumbbell Row',
		'primary_muscle' => '2',
		'secondary_muscle' => '7',
		'third_muscle' => '3',
		'fourth_muscle' => '6',
		'demonstration_url' => '--gDUDFKx6Q',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Lying T-Bar Row',
		'primary_muscle' => '2',
		'secondary_muscle' => '7',
		'third_muscle' => '3',
		'fourth_muscle' => '6',
		'demonstration_url' => 'w0KnlQ-b7jw',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//LATS
		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Wide-Grip Lat Pulldown',
		'primary_muscle' => '3',
		'secondary_muscle' => '7',
		'third_muscle' => '2',
		'fourth_muscle' => '6',
		'demonstration_url' => 'lueEJGjTuPQ',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Close Grip Front Lat Pulldown',
		'primary_muscle' => '3',
		'secondary_muscle' => '7',
		'third_muscle' => '2',
		'fourth_muscle' => '6',
		'demonstration_url' => 'ecRF8ERf2q4',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Chin-Ups',
		'primary_muscle' => '3',
		'secondary_muscle' => '7',
		'third_muscle' => '2',
		'fourth_muscle' => '9',
		'demonstration_url' => 'fNZ8Ty7jSFA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Pull-Ups',
		'primary_muscle' => '3',
		'secondary_muscle' => '7',
		'third_muscle' => '2',
		'fourth_muscle' => '9',
		'demonstration_url' => 'WXMKjV11lAk',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Straight-Arm Pulldown',
		'primary_muscle' => '3',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'WXMKjV11lAk',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));
		
		//LOWER
		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Barbell Deadlift',
		'primary_muscle' => '4',
		'secondary_muscle' => '13',
		'third_muscle' => '12',
		'fourth_muscle' => '11',
		'demonstration_url' => 'G13343CGcPs',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Hyper Back Extenstions',
		'primary_muscle' => '4',
		'secondary_muscle' => 'Abs',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'G13343CGcPs',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Rack Pulls',
		'primary_muscle' => '4',
		'secondary_muscle' => '9',
		'third_muscle' => '11',
		'fourth_muscle' => '13',
		'demonstration_url' => 'u7NE34Vw81w',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Superman',
		'primary_muscle' => '4',
		'secondary_muscle' => 'Abs',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'hhq86gJvrvo',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//5 EXERCISE----------------------------
		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Flat Bench Press',
		'primary_muscle' => '5',
		'secondary_muscle' => '8',
		'third_muscle' => '6',
		'fourth_muscle' => '',
		'demonstration_url' => 'Qjxrp9Hwv_Q',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Close Grip Bench Press',
		'primary_muscle' => '5',
		'secondary_muscle' => '8',
		'third_muscle' => '6',
		'fourth_muscle' => '',
		'demonstration_url' => 'OYoc93qAAEY',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));


		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Incline Bench Press',
		'primary_muscle' => '5',
		'secondary_muscle' => '8',
		'third_muscle' => '6',
		'fourth_muscle' => '',
		'demonstration_url' => '8YgkJN0gmNM',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Dumbbell Bench Press',
		'primary_muscle' => '5',
		'secondary_muscle' => '8',
		'third_muscle' => '6',
		'fourth_muscle' => '',
		'demonstration_url' => 'Vc63DPUoA40',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));


		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Dumbbell Incline Press',
		'primary_muscle' => '5',
		'secondary_muscle' => '8',
		'third_muscle' => '6',
		'fourth_muscle' => '',
		'demonstration_url' => 'DnV3R4vp3K0',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Dumbbell Decline Press',
		'primary_muscle' => '5',
		'secondary_muscle' => '8',
		'third_muscle' => '6',
		'fourth_muscle' => '',
		'demonstration_url' => 'Pf1nDoqx_1A',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Bent-Arm Dumbbell Pull Over',
		'primary_muscle' => '5',
		'secondary_muscle' => '8',
		'third_muscle' => '6',
		'fourth_muscle' => '3',
		'demonstration_url' => 'Pf1nDoqx_1A',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Dumbbell Flyes',
		'primary_muscle' => '5',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'QwuUZ5wgQOk',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Push Ups',
		'primary_muscle' => '5',
		'secondary_muscle' => '6',
		'third_muscle' => '8',
		'fourth_muscle' => '',
		'demonstration_url' => 'XIHO5t_VBPQ',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Cable Crossover',
		'primary_muscle' => '5',
		'secondary_muscle' => '6',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'aoP0s_MjN-g',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Dips Chest Version',
		'primary_muscle' => '5',
		'secondary_muscle' => '6',
		'third_muscle' => '8',
		'fourth_muscle' => '',
		'demonstration_url' => 'aoP0s_MjN-g',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '2',
		'name' => 'Butterfly Press',
		'primary_muscle' => '5',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'oGxc2ph8Fnw',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//SHOULDER EXERCISE----------------------------

		Exercise::create(array(

		'id' => '',
		'body_part_id' => '3',
		'name' => 'Clean and Press',
		'primary_muscle' => '6',
		'secondary_muscle' => '4',
		'third_muscle' => '12',
		'fourth_muscle' => '13',
		'demonstration_url' => 'pajZn-M-r4Q',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Barbell Shoulder Press',
		'primary_muscle' => '6',
		'secondary_muscle' => '5',
		'third_muscle' => '8',
		'fourth_muscle' => '',
		'demonstration_url' => 'lPFwcHl0a2c',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Push Press',
		'primary_muscle' => '6',
		'secondary_muscle' => '12',
		'third_muscle' => '8',
		'fourth_muscle' => '',
		'demonstration_url' => 'ChTn_TLDA5o',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Barbell Rear Delt Row',
		'primary_muscle' => '6',
		'secondary_muscle' => '7',
		'third_muscle' => '3',
		'fourth_muscle' => '2',
		'demonstration_url' => 'XagV4XfG9fo',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Seated Bent Over Rear Delt Raise',
		'primary_muscle' => '6',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'DEeUsQ9rCso',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Dumbbell Shoulder Press',
		'primary_muscle' => '6',
		'secondary_muscle' => '8',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'tzZMsrzG_zE',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Standing Military Press',
		'primary_muscle' => '6',
		'secondary_muscle' => '8',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => '8E4oWpi0RkA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Seated Barbell Military Press',
		'primary_muscle' => '6',
		'secondary_muscle' => '8',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'AFC-_Ka8nY4',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Arnold Dumbbell Press',
		'primary_muscle' => '6',
		'secondary_muscle' => '8',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'X60-yTMOJfw',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Side Lateral Raises',
		'primary_muscle' => '6',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'LT1Eo-q58yg',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Upright Barbell Row',
		'primary_muscle' => '6',
		'secondary_muscle' => '1',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'KzYlXWXVw3o',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Standing Dumbbell Upright Row',
		'primary_muscle' => '6',
		'secondary_muscle' => '7',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'SO_nHq52a8o',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Reverse Flyes',
		'primary_muscle' => '6',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'WCvRMULhUVU',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Clean and Jerk',
		'primary_muscle' => '6',
		'secondary_muscle' => '4',
		'third_muscle' => '12',
		'fourth_muscle' => '1',
		'demonstration_url' => 'v13d7g_uUXM',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '3',
		'name' => 'Hang Clean',
		'primary_muscle' => '6',
		'secondary_muscle' => '4',
		'third_muscle' => '12',
		'fourth_muscle' => '11',
		'demonstration_url' => 'v13d7g_uUXM',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));


		//ARM EXERCISE----------------------------
		//BICEP
		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Barbell Curl',
		'primary_muscle' => '7',
		'secondary_muscle' => '9',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'dDI8ClxRS04',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Hammer Curl',
		'primary_muscle' => '7',
		'secondary_muscle' => '9',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => '0IAM2YtviQY',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Preacher Curl',
		'primary_muscle' => '7',
		'secondary_muscle' => '9',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'RgN216Cumtw',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Concentration Curl',
		'primary_muscle' => '7',
		'secondary_muscle' => '9',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'ZcU2hN76UyA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Spider Curl',
		'primary_muscle' => '7',
		'secondary_muscle' => '9',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'TVjOooXvzO8',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Dumbbell Curl',
		'primary_muscle' => '7',
		'secondary_muscle' => '9',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => '3OZ2MT_5r3Q',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'One Arm Dumbbell Preacher Curl',
		'primary_muscle' => '7',
		'secondary_muscle' => '9',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'WK5yZMlgMb4',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//TRICEPS
		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Tricep Dips',
		'primary_muscle' => '8',
		'secondary_muscle' => '5',
		'third_muscle' => '6',
		'fourth_muscle' => '',
		'demonstration_url' => 'WNMqN2O2MSs',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Dumbbell Extensions',
		'primary_muscle' => '8',
		'secondary_muscle' => '5',
		'third_muscle' => '6',
		'fourth_muscle' => '',
		'demonstration_url' => 'ntBjdnckWgo',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Tricep Pushdowns',
		'primary_muscle' => '8',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'HIKzvHkibWc',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Tricep Dumbbell Kickback',
		'primary_muscle' => '8',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'HyqTb_jE_oI',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Cable Rope Overhead Tricep Extension',
		'primary_muscle' => '8',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'mRozZKkGIfg',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//FOREARM
		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Wrist Roller',
		'primary_muscle' => '9',
		'secondary_muscle' => '6',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'mRozZKkGIfg',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Behind The Back Wrist Curl',
		'primary_muscle' => '9',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'sVLVLcsfWSo',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '4',
		'name' => 'Farmer Walk',
		'primary_muscle' => '9',
		'secondary_muscle' => '4',
		'third_muscle' => '12',
		'fourth_muscle' => '13',
		'demonstration_url' => 'sVLVLcsfWSo',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//AB EXERCISE----------------------------
		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Standing Cable Wood Chop',
		'primary_muscle' => '10',
		'secondary_muscle' => '6',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'iTmMPyDzeYA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Cable Crunch',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => '3qjoXDTuyOE',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Decline Reverse Crunch',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'YcMj0EEadQo',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Dumbbell Side Bend',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'UUQHeBRE_Wo',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Sit Ups',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => '10mp1wusuzM',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Reverse Crunch',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'lmSP-c1X_iY',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Ab Crunch Machine',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'JSBuaT1tHfM',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Barbell Side Bend',
		'primary_muscle' => '10',
		'secondary_muscle' => '4',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => '9_kAFDM6l6o',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Jackknife Sit Up',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'GEZ8NLbtc8Q',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Hanging Leg Raise',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'uaxEVt2z4CE',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Stomach Vacuum',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'f2PliOQHQ4c',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Crunches',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'YdZakh0Pkwc',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Ab Roller',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'Q5MT5omGNJI',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Air Bike',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'i6mPCVUrtNk',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Plank',
		'primary_muscle' => '10',
		'secondary_muscle' => '6',
		'third_muscle' => '4',
		'fourth_muscle' => '',
		'demonstration_url' => 'tgbrMdfuGJA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '5',
		'name' => 'Russian Twist',
		'primary_muscle' => '10',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'tgbrMdfuGJA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));


		//LEGS EXERCISE----------------------------
		//QUADS
		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Hack Squats',
		'primary_muscle' => '12',
		'secondary_muscle' => '13',
		'third_muscle' => '14',
		'fourth_muscle' => '9',
		'demonstration_url' => 'plv5ur26Q7A',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Dumbbell Squats',
		'primary_muscle' => '12',
		'secondary_muscle' => '11',
		'third_muscle' => '13',
		'fourth_muscle' => '14',
		'demonstration_url' => 'r9gqv3WF90I',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Front Barbell Squat',
		'primary_muscle' => '12',
		'secondary_muscle' => '11',
		'third_muscle' => '13',
		'fourth_muscle' => '14',
		'demonstration_url' => '9Hi_sgKPNEo',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Freehand Jump Squat',
		'primary_muscle' => '12',
		'secondary_muscle' => '11',
		'third_muscle' => '13',
		'fourth_muscle' => '14',
		'demonstration_url' => 'dtJ6kOV5dUc',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Barbell Squat',
		'primary_muscle' => '12',
		'secondary_muscle' => '11',
		'third_muscle' => '13',
		'fourth_muscle' => '14',
		'demonstration_url' => 'tVB1q8zkP3o',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Leg Press',
		'primary_muscle' => '12',
		'secondary_muscle' => '13',
		'third_muscle' => '11',
		'fourth_muscle' => '14',
		'demonstration_url' => '3R0SOJ3alTA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Dumbbell Step Ups',
		'primary_muscle' => '12',
		'secondary_muscle' => '13',
		'third_muscle' => '14',
		'fourth_muscle' => '11',
		'demonstration_url' => '7AtIjR-QqVA',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Leg Extensions',
		'primary_muscle' => '12',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'yR_LqZYSIgM',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Split Squat',
		'primary_muscle' => '12',
		'secondary_muscle' => '11',
		'third_muscle' => '13',
		'fourth_muscle' => '4',
		'demonstration_url' => 'UJWLxHAYxx4',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Barbell Lunges',
		'primary_muscle' => '12',
		'secondary_muscle' => '11',
		'third_muscle' => '13',
		'fourth_muscle' => '14',
		'demonstration_url' => 'Wb8Yr3Nx7dE',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));
		
		//GLUTES
		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Butt Lift',
		'primary_muscle' => '11',
		'secondary_muscle' => '13',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'URhjZLFfKMs',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Barbell Hip Thrust',
		'primary_muscle' => '11',
		'secondary_muscle' => '13',
		'third_muscle' => '14',
		'fourth_muscle' => '',
		'demonstration_url' => 'Fk1OfkMmVt4',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'One-Legged Cable Kickback',
		'primary_muscle' => '11',
		'secondary_muscle' => '13',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'e1pFg9Rz55k',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//HAMSTRING
		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Glute Ham Raise',
		'primary_muscle' => '13',
		'secondary_muscle' => '11',
		'third_muscle' => '14',
		'fourth_muscle' => '',
		'demonstration_url' => 'TDdV0dCsqKs',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Lying Leg Curls',
		'primary_muscle' => '13',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'jxctD6fL_FQ',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Stiff-Legged Deadlift',
		'primary_muscle' => '13',
		'secondary_muscle' => '11',
		'third_muscle' => '4',
		'fourth_muscle' => '',
		'demonstration_url' => 'NzMDStjQadQ',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Stiff Legged Dumbbell Deadlift',
		'primary_muscle' => '13',
		'secondary_muscle' => '11',
		'third_muscle' => '4',
		'fourth_muscle' => '',
		'demonstration_url' => 'w9_PudlkeLI',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Romanian Deadlift',
		'primary_muscle' => '13',
		'secondary_muscle' => '11',
		'third_muscle' => '4',
		'fourth_muscle' => '14',
		'demonstration_url' => 'e1pFg9Rz55k',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Kettlebell One-Legged Deadlift',
		'primary_muscle' => '13',
		'secondary_muscle' => '11',
		'third_muscle' => '4',
		'fourth_muscle' => '14',
		'demonstration_url' => 'JOoc07_Xkls',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Power Clean',
		'primary_muscle' => '13',
		'secondary_muscle' => '11',
		'third_muscle' => '4',
		'fourth_muscle' => '12',
		'demonstration_url' => 'zCEj0d3TatI',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//CALVES
		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Standing Calf Raises',
		'primary_muscle' => '14',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'MAMzF7iZNkc',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Standing Dumbbell Calf Raises',
		'primary_muscle' => '14',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'wxwY7GXxL4k',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Exercise::create(array(
		
		'id' => '',
		'body_part_id' => '6',
		'name' => 'Donkey Calf Raises',
		'primary_muscle' => '14',
		'secondary_muscle' => '',
		'third_muscle' => '',
		'fourth_muscle' => '',
		'demonstration_url' => 'XEZUIXn5mgc',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

	}

}