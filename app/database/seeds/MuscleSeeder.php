<?php

class MuscleSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('muscle')->delete();

		//BACK MUSCLES----------------------------
		Muscle::create(array(

		'id' => '',
		'body_part_id' => '1',
		'name' => 'Trapezius',
		'slug' => 'trapezius',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		Muscle::create(array(

			'id' => '',
			'body_part_id' => '1',
			'name' => 'Middle Back',
			'slug' => 'middleback',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		Muscle::create(array(

			'id' => '',
			'body_part_id' => '1',
			'name' => 'Lats',
			'slug' => 'lats',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		Muscle::create(array(

			'id' => '',
			'body_part_id' => '1',
			'name' => 'Lower Back',
			'slug' => 'lowerback',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		//CHEST MUSCLES----------------------------
		Muscle::create(array(

			'id' => '',
			'body_part_id' => '2',
			'name' => 'Pectorals',
			'slug' => 'pectorals',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		//SHOULDER MUSCLES----------------------------
		Muscle::create(array(

			'id' => '',
			'body_part_id' => '3',
			'name' => 'Deltoids',
			'slug' => 'deltoids',
			'created_at' => new DateTime,
            'updated_at' => new DateTime,

		));

		//ARM MUSCLES----------------------------
		Muscle::create(array(

			'id' => '',
			'body_part_id' => '4',
			'name' => 'Biceps',
			'slug' => 'biceps',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		Muscle::create(array(

			'id' => '',
			'body_part_id' => '4',
			'name' => 'Triceps',
			'slug' => 'triceps',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		Muscle::create(array(

			'id' => '',
			'body_part_id' => '4',
			'name' => 'Forearms',
			'slug' => 'forearms',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		//AB MUSCLES----------------------------
		Muscle::create(array(

		'id' => '',
		'body_part_id' => '5',
		'name' => 'Abdominals',
		'slug' => 'abdominals',
		'created_at' => new DateTime,
        'updated_at' => new DateTime

		));

		//LEG MUSCLES----------------------------
		Muscle::create(array(

			'id' => '',
			'body_part_id' => '6',
			'name' => 'Glutes',
			'slug' => 'glutes',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		Muscle::create(array(

			'id' => '',
			'body_part_id' => '6',
			'name' => 'Quadriceps',
			'slug' => 'quadriceps',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		Muscle::create(array(

			'id' => '',
			'body_part_id' => '6',
			'name' => 'Hamstrings',
			'slug' => 'hamstrings',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

		Muscle::create(array(

			'id' => '',
			'body_part_id' => '6',
			'name' => 'Calves',
			'slug' => 'calves',
			'created_at' => new DateTime,
            'updated_at' => new DateTime

		));

	}

}