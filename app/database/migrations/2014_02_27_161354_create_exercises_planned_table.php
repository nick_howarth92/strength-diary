<?php

use Illuminate\Database\Migrations\Migration;

class CreateExercisesPlannedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exercises_planned', function($table){

			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('session_id')->references('id')->on('sessions'); 
			$table->integer('exercise_id')->references('id')->on('exercises');
			$table->integer('type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exercises_planned');
	}

}