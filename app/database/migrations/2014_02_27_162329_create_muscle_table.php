<?php

use Illuminate\Database\Migrations\Migration;

class CreateMuscleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('muscle', function($table){

			$table->increments('id');
			$table->integer('body_part_id')->references('id')->on('body_part'); 
			$table->string('name');
			$table->string('slug');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('muscle');
	}
}