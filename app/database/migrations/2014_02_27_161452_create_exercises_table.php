<?php

use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exercises', function($table){

			$table->increments('id');
			$table->integer('body_part_id')->references('id')->on('body_part');
			$table->string('name'); 
			$table->integer('primary_muscle')->references('id')->on('muscle'); 
			$table->integer('secondary_muscle')->references('id')->on('muscle'); 
			$table->integer('third_muscle')->references('id')->on('muscle'); 
			$table->integer('fourth_muscle')->references('id')->on('muscle');
			$table->string('demonstration_url');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exercises');
	}

}