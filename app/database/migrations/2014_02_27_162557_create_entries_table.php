<?php

use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entries', function($table){

			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('exercise_planned_id')->references('exercise_id')->on('exercises_planned'); 
			$table->decimal('total_weight');
			$table->integer('total_reps');
			$table->integer('effort');
			$table->decimal('score');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entries');
	}

}