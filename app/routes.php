<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::filter('auth', function()
{
  
  //check if a user is logged in
  if (!Auth::check() ) {
  	return Redirect::to('/');
  }

});

//making the views with cotrollers
Route::get('/', 'HomeController@index');

//routes for signing up
Route::get('signup', 'SignupController@index');
Route::post('signup-submission', 'SignupController@signup');

Route::get('login', 'LoginController@index');
Route::post('login', 'LoginController@login');

Route::get('logout', 'LogoutController@index');
Route::get('logout', 'LogoutController@logout');

//profile routes
Route::get('profile', 'AppProfileController@index');
//uploading a picture
Route::post('uploadImage', 'ProfilePictureController@uploadImage');

//modal view for profile picture
Route::get('editprofilepicture', 'ProfilePictureController@index');

//All routes for schedule page
Route::get('schedule', array('before' => 'auth', 'uses' => 'AppScheduleController@index'));
Route::get('schedule/muscleByPart/{id}','AppScheduleController@showMuscles');
Route::get('schedule/exerciseByPart/{id}','AppScheduleController@showExercises');
Route::get('schedule/indivdualExercise/{id}','AppScheduleController@showIndivExercise');
//deals with adding of session
Route::post('addsession', 'AppScheduleController@addSession');
//deals with adding of score
Route::post('addscore', 'AppScheduleController@addScore');

//show the video in question on the schedule page
Route::get('showvideo/{id}', 'showVideoController@index');

//loads page to enter score
Route::get('exercisescore/{id}', 'exerciseScoreController@index');

//All routes for progression page
Route::get('progression', array('before' => 'auth', 'uses' => 'AppProgressionController@index'));
Route::get('progression/muscleByPart/{id}','AppProgressionController@showMuscles');
Route::get('progression/exerciseByPart/{id}','AppProgressionController@showExercises');
Route::get('progression/exerciseScore/{id}/type/{type}','AppProgressionController@exerciseScore');

// set up a url which looks for a catch on the id
Route::get('demonstration', array('before' => 'auth', 'uses' => 'AppDemonstrationController@index'));  
Route::get('demonstration/exerciseByPart/{id}','AppDemonstrationController@showExercises');  
Route::get('demonstration/muscleByPart/{id}','AppDemonstrationController@showMuscles');

//route for google calendar
Route::get('exportGoogleCal', array('before' => 'auth', 'uses' =>'GoogleCalendarController@addToCalendar'));



